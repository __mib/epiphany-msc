#include <stdio.h>
#include <stdlib.h>
#include "e-lib.h"

#include "necro.h"

volatile task_status status SECTION("shared_dram");
unsigned local_timer0_high SECTION(".mibinternal.local_timer0_high");
unsigned local_timer1_high SECTION(".mibinternal.local_timer1_high");

void __attribute__((interrupt)) SECTION(".mibinternal.timer0_isr") timer0_isr(void) {
    ++local_timer0_high;
    e_ctimer_set(E_CTIMER_0, E_CTIMER_MAX);
}

void __attribute__((interrupt)) SECTION(".mibinternal.timer1_isr") timer1_isr(void) {
    ++local_timer1_high;
    e_ctimer_set(E_CTIMER_1, E_CTIMER_MAX);
}

extern int Main(int);

int main() {
    while(status.status != 's');
    status.timer0_hl[0] = status.timer0_hl[1] = 0;
    status.timer1_hl[0] = status.timer1_hl[1] = 0;
    
    local_timer0_high = local_timer1_high = 0;
    status.status = 'r';
    
    e_irq_attach(E_TIMER0_INT, (sighandler_t) timer0_isr);
    e_irq_attach(E_TIMER1_INT, (sighandler_t) timer1_isr);
    
    e_irq_mask(E_TIMER0_INT, E_FALSE);
    e_irq_mask(E_TIMER1_INT, E_FALSE);
    e_irq_global_mask(E_FALSE);

    e_ctimer_set(E_CTIMER_0, E_CTIMER_MAX);
    e_ctimer_set(E_CTIMER_1, E_CTIMER_MAX);
    
    e_ctimer_start(E_CTIMER_0, E_CTIMER_CLK);
    e_ctimer_start(E_CTIMER_1, E_CTIMER_EXT_LOAD_STALLS);
    
    status.result = Main(status.arg);

    e_ctimer_stop(E_CTIMER_0);
    e_ctimer_stop(E_CTIMER_1);

    e_irq_global_mask(E_TRUE);
    e_irq_mask(E_TIMER0_INT, E_TRUE);
    e_irq_mask(E_TIMER1_INT, E_TRUE);
    
    status.timer0_hl[0] = E_CTIMER_MAX - e_ctimer_get(E_CTIMER_0);
    status.timer0_hl[1] = local_timer0_high;    

    status.timer1_hl[0] = E_CTIMER_MAX - e_ctimer_get(E_CTIMER_1);
    status.timer1_hl[1] = local_timer1_high;    

    status.status = 'f';

    __asm__ __volatile__("idle");
}

