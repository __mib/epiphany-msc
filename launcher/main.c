#include <e-hal.h>

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "necro.h"

e_platform_t platform;
e_epiphany_t device;
e_mem_t status_buffer;
char srec_filename[256];
task_status status;
unsigned int status_size;
unsigned int original_arg;

void uploadStatus() {
    e_write(&status_buffer, 0, 0, 0, &status, status_size);
}

void downloadStatus() {
    e_read(&status_buffer, 0, 0, 0, &status, status_size);
}

void printStatus() {
    printf("SREC: %s OArg: %u Status: %c Arg: %u Timer0: %llu Timer1: %llu Result: %d\n", 
            srec_filename, original_arg,
            status.status, status.arg, status.timer0, status.timer1, status.result);
}

int main() {
    fprintf(stderr, "SREC: ");
    scanf("%s", srec_filename);
    fprintf(stderr, "  srec: [%s]\n", srec_filename);
    
    fprintf(stderr, "ARG: ");
    scanf("%u", &status.arg);
    original_arg = status.arg;
    fprintf(stderr, "  arg: %u\n", status.arg);

    status_size = sizeof(task_status);
    fprintf(stderr, "Status struct size: %u\n", status_size);


    fprintf(stderr, "Initializing...\n");
    assert(0 == e_init(0));
    e_reset_system();
    e_get_platform_info(&platform);
    fprintf(stderr, "Platform version: [%s]\n", platform.version);

    assert(0 == e_alloc(&status_buffer, 0x01000000, status_size));


    fprintf(stderr, "Opening device...\n");
    assert(0 == e_open(&device, 0, 0, 1, 1));

    fprintf(stderr, "Resetting device...\n");
    e_reset_group(&device);
    e_load(srec_filename, &device, 0, 0, 1);

    status.status = 's';
    uploadStatus();

    while(1) {
        downloadStatus();
        if (status.status == 'f') break;
        usleep(100000);
    }

    printStatus();
    
    e_close(&device);
    e_free(&status_buffer);
    e_finalize();
    return 0;
}

