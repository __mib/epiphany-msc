#!/bin/bash

set -e
set -x

ESDK=${EPIPHANY_HOME}
ELIBS=${ESDK}/tools/host/lib
EINCS=${ESDK}/tools/host/include
ELDF=task_loop.ldf

e-gcc -T ${ELDF} task.c task_loop.c -o task_loop.elf -le-lib

e-objcopy --srec-forceS3 --output-target srec task_loop.elf task_loop.srec
