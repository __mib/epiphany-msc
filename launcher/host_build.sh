#!/bin/bash

set -e
set -x

ESDK=${EPIPHANY_HOME}
ELIBS=${ESDK}/tools/host/lib
EINCS=${ESDK}/tools/host/include
ELDF=${ESDK}/bsps/current/fast.ldf

gcc main.c -o launcher -I ${EINCS} -L ${ELIBS} -le-hal
