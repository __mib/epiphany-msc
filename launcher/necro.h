typedef struct __attribute__((__packed__)) {
    char status;
    unsigned int arg;
    
    union {
        unsigned int timer0_hl[2];
        unsigned long long timer0;
    };

    union {
        unsigned int timer1_hl[2];
        unsigned long long timer1;
    };

    int result;
} task_status;

