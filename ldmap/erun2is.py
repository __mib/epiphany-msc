"""
Resolves erun.trace file to input section names.
Arguments: linker_map, trace_file, (optional: total size trace_file in bytes, 
    for reporting purposes only)

Output format:
instr_count input_section_name
"""

import sys, re, itertools
import pprint
from ldmap import LinkerMap, cached

m = LinkerMap(open(sys.argv[1]))
log = []

REPORT_FREQ = 100000

total_size = 0
if len(sys.argv) > 3:
    total_size = int(sys.argv[3]) 

cnt = 0
byte_cnt = 0
output_cnt = 0

line_pattern = re.compile(r'read-\d+ exec:')

last_entry = None
instr_count = 0

@cached
def get_section_name(i):
    return m.get_input_sections(i)

def print_entry(e):
    global output_cnt
    if instr_count:
        output_cnt += 1
        print instr_count, section_name

for line in open(sys.argv[2]):
    cnt += 1
    byte_cnt += len(line)
    if line_pattern.match(line):
        prefix, rest = line.strip().split(':')
        a, d, b = rest.split()
        i = int(a, 16)

        # entry = (prefix, m.get_regions(i), m.get_output_sections(i), m.get_input_sections(i))
        isecs = get_section_name(i)
        
        section_name = ""

        if isecs != []:
            section_name = isecs[0].name

        if len(isecs) != 1:
            # print >> sys.stderr, "get_input_sections did not return a single result for: ", line, isecs
            isecs = ['?']
            section_name = "?"

        entry = (section_name,)

        if entry == last_entry:
            instr_count += 1
        else:
            print_entry(last_entry)
            last_entry = entry
            instr_count = 1

    if cnt % REPORT_FREQ == 0:
        print >> sys.stderr, "\r",
        if total_size:
            print >> sys.stderr, "{:10.4f}%".format(100.0 * byte_cnt / total_size),
        print >> sys.stderr, "\t", \
            "{:10.1f} M lines read".format(cnt / 1000000.0), \
            "\t", "{:10.1f} K output lines".format(output_cnt / 1000.0),

print_entry(last_entry)
