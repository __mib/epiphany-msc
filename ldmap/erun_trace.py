import sys
from ldmap import LinkerMap

m = LinkerMap(open(sys.argv[1]))

#for x in m.regions:
#    print x

def f(a):
    i = int(a, 16)
    return "R: ", m.get_region(i).name, " S ", m.get_output_sections(i), "(%s)" % a

for line in open(sys.argv[2]):
    prefix, rest = line.strip().split(':')
    a, d, b = rest.split()
    print prefix, f(a), d, f(b) 
