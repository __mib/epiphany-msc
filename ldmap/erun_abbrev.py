import sys, re, itertools
import pprint
from ldmap import LinkerMap

m = LinkerMap(open(sys.argv[1]))
log = []

sys.stderr.write("Resolving\n")
cnt = 0

for line in open(sys.argv[2]):
    cnt += 1
    if re.match(r'read-\d+ exec:', line):
        prefix, rest = line.strip().split(':')
        a, d, b = rest.split()
        i = int(a, 16)
        log.append( (prefix, m.get_regions(i), m.get_output_sections(i), m.get_input_sections(i)) ) 
    if cnt % 10000 == 0:
        sys.stderr.write(".")
        sys.stderr.flush()

sys.stderr.write("\nAddresses resolved\n")

pp = pprint.PrettyPrinter()

def mib(x):
    print '\n'.join("      " + x for x in 
            pp.pformat(dict(x._asdict().items())).split('\n'))

def pick_isection(isections, osection):
    if not isections:
        return "[?]"
    suf = " (...)" if len(isections) > 1 else ""
    for i in isections:
        if i.output_section == osection:
            return i.name + suf
    return isections[0].name + suf

#Short version
short = []
for k, g in itertools.groupby(log):
    g = list(g)
    p, r, o, i = k
    
    sr = 'OTHER'
    si = ''
    for x in r:
        if x.name.startswith('EXTERNAL'):
            sr = 'EXTERNAL_RAM'
            si = pick_isection(i, x)
    for x in r:
        if x.name == 'INTERNAL_RAM':
            sr = 'INTERNAL_RAM'
            si = ''
    for x in o:
        if x.name == '.mibstubs':
            sr = 'MIBSTUB'
            si = pick_isection(i, x)

    short.append( (len(g), sr, si) )

for k, g in itertools.groupby(short, key=lambda x: x[1:]):
    cnt = sum(map(lambda x: x[0], list(g)))
    print '{0:7d} {1:14s} {2}'.format(cnt, *k)

#Long version
print
print
print "-"*80
print
print

for k, g in itertools.groupby(log):
    g = list(g)
    p, r, o, i = k
    print p 
    print "  Count:", len(g)
    print "    Regions:"
    for x in r:
        if x.name != '*default*':
            mib(x)
    print "    Output sections:"
    for x in o:
        mib(x)
    print "    Input sections:"
    for x in i:
        mib(x)
