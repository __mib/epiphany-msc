"""
Module for reading ld output map.

Also, some helper functions.
"""

import collections
import itertools
import re
import bisect
import sys

def humanized(i):
    for s in ['', 'K', 'M', 'G', 'T']:
        if i >= 1024.0:
            i /= 1024.0
        else:
            return "{:.2f}".format(i) + s
    return "inf"

class cached(object):
    def __init__(self, f):
        self.f = f
        self.cache = dict()

    def __call__(self, *args):
        if args in self.cache:
            return self.cache[args]
        r = self.f(*args)
        self.cache[args] = r
        return r

# (1, 2) (2, 3) (3, 4)
def pairwise(iterable):
    a, b = itertools.tee(iterable)
    next(b, None)
    return itertools.izip(a, b)

MemoryRegion = collections.namedtuple(
        'MemoryRegion', ['name', 'origin', 'length', 'attrs'])

InputSection = collections.namedtuple(
        'InputSection', ['name', 'origin', 'length', 'output_section',
            'pattern', 'desc'])

OutputSection = collections.namedtuple(
        'OutputSection', ['name', 'origin', 'length', 'load_address'])

Symbol = collections.namedtuple(
        'Symbol', ['addr', 'name', 'input_section', 'output_section'])

class LinkerMap(object):
   
    ADDR_UPPER_BOUND = 0xffffffffffffffff + 0x1

    def __init__(self, lines):
        self.active_handler = self.default_handler
        self.cues = {
            "Memory Configuration": self.memcfg_handler,
            "Linker script and memory map": self.memory_map_handler,
        }
        self.content = lines
        self.regions = []
        self.output_sections = []
        self.input_sections = []
        self.symbols = []
        self.active_output_section = None
        self.active_input_section = None
        self.memory_map_header_seen = False
        self.mm_last_tn = None
        self.error_log = []

        try:
            while True:
                line = self.content.next()
                self.dispatch_handler(line)
        except StopIteration:
            pass

        self.build_resolv_maps()

    def build_resolv_maps(self):
        self.symbol_lookup = collections.defaultdict(list)
        for x in self.symbols:
            self.symbol_lookup[x.addr].append(x)

        pruned = ['.debug', '.stab', '.comment']
        self.input_sections = filter(lambda i: i.origin != None and \
                not any([i.name.startswith(x) for x in pruned]),
                self.input_sections)
        self.output_sections = filter(lambda i: i.origin != None and \
                not any([i.name.startswith(x) for x in pruned]),
                self.output_sections)

        P = list(itertools.chain.from_iterable(
                [(s.origin, s.origin + s.length) for s in 
                itertools.chain(self.input_sections, 
                    self.output_sections)]))
        P = sorted(list(set(P)))
        I = collections.defaultdict(list)
        O = collections.defaultdict(list)
        for (a, b) in pairwise(P):
            for s in self.input_sections:
                if s.origin <= a and s.origin + s.length >= b:
                    I[(a, b)].append(s)
            for s in self.output_sections:
                if s.origin <= a and s.origin + s.length >= b:
                    O[(a, b)].append(s)
        self.input_section_lookup = sorted(I.items())
        self.output_section_lookup = sorted(O.items())
        self.isection_name_lookup = collections.defaultdict(list)

        for i in self.input_sections:
            self.isection_name_lookup[i.name].append(i)

    def get_isection_for_name(self, isection_name):
        return self.isection_name_lookup[isection_name]

    def get_symbols(self, addr):
        return self.symbol_lookup[addr]

    def dispatch_handler(self, line):
        if line.strip() in self.cues:
            self.active_handler = self.cues[line.strip()]
        else:
            try:
                self.active_handler(line)
            except (ValueError, IndexError) as e:
                self.error_log.append((line, e))

    def default_handler(self, line):
        pass

    def make_region(self, name, origin, length, attrs=''):
        self.regions.append(
                MemoryRegion(name, int(origin, 16), int(length, 16), attrs))

    def memcfg_handler(self, line):
        assert line.strip() == ''
        assert re.match(r"Name\s+Origin\s+Length\s+Attributes", self.content.next().strip())
        for line in itertools.takewhile(str.strip, self.content):
            self.make_region(*line.strip().split())
        self.active_handler = self.default_handler

    def get_regions(self, addr):
        regions = filter(
                lambda r: r.origin <= addr and addr < r.origin + r.length,
                self.regions)
        return sorted(regions, key=lambda x: x.length)

    def get_region(self, addr):
        return self.get_regions(addr)[0]

    def _get_sections(self, addr, lookup):
        key = ((addr, LinkerMap.ADDR_UPPER_BOUND), None)
        i = bisect.bisect_left(lookup, key) - 1
        (a, b), s = lookup[i]
        return sorted(s)

    def get_output_sections(self, addr):
        return self._get_sections(addr, self.output_section_lookup)

    def get_input_sections(self, addr):
        return self._get_sections(addr, self.input_section_lookup)

    def _legacy_get_output_sections(self, addr):
        regions = filter(
                lambda r: r.origin != None and r.length != None and \
                          r.origin <= addr and addr < r.origin + r.length,
                self.output_sections)
        load_regions = filter(
                lambda r: r.load_address != None and r.length != None and \
                          r.load_address <= addr and addr < r.load_address + r.length,
                self.output_sections)
        return regions + load_regions

    def _legacy_get_input_sections(self, addr):
        regions = filter(
                lambda r: r.origin != None and r.length != None and \
                          r.origin <= addr and addr < r.origin + r.length,
                self.input_sections)
        return regions

    def memory_map_handler(self, line):
        line = line.rstrip()
        if not self.memory_map_header_seen:
            assert line == ''
            self.content = itertools.dropwhile(lambda s: s.startswith('LOAD '), self.content)
            
            self.memory_map_header_seen = True
            return
        if not line:
            self.active_output_section = self.active_input_section = self.active_pattern = None
            self.mm_last_tn = None
            return
        if line[0] != ' ': # output section
            tokens = line.strip().split()
            name = tokens[0]
            length = origin = load_address = None
            if len(tokens) > 1:
                origin = int(tokens[1], 16)
                length = int(tokens[2], 16)
            la_match = re.search(r'load address (0x\w+)', line)
            if la_match:
                load_address = int(la_match.group(1), 16)
            self.active_output_section = OutputSection(name, origin, length, load_address)
            self.output_sections.append(self.active_output_section)
            return

        def handle_input_section(tokens): 
            if self.mm_last_tn:
                self.active_pattern = self.mm_last_tn
            self.mm_last_tn = None
            self.active_input_section = InputSection(
                    tokens[0], int(tokens[1], 16), int(tokens[2], 16), 
                    self.active_output_section, self.active_pattern,
                    tokens[3] if len(tokens) > 3 else None)
            self.input_sections.append(self.active_input_section)
            return
        
        if line[:2] != '  ':
            tokens = line.strip().split()
            try:
                handle_input_section(tokens)
                return
            except (ValueError, IndexError): # just another lonely token
                self.mm_last_tn = line.strip()
                return
        
        tokens = line.strip().split()

        def is_hex(s):
            return s.startswith('0x')
        
        if self.mm_last_tn and is_hex(tokens[0]):
            # this line contains continuation of an input section description
            tokens = [self.mm_last_tn,] + tokens 
            self.mm_last_tn = None
            handle_input_section(tokens)
            return

        tokens = line.strip().split(' ', 1)
        symbol = Symbol(int(tokens[0], 16), tokens[1].strip(),
                self.active_input_section, self.active_output_section)
        self.symbols.append(symbol)
        self.mm_last_tn = None
