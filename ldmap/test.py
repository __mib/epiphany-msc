ldf = """
Archive member included because of file (symbol)

libc.a(lib_a-exit.o)          /home/mib/ephiphany-sdk/esdk/tools/e-gnu.armv7l/epiphany-elf/lib/crt0.o (exit)
libc.a(lib_a-fstatr.o)        stubs.o (_fstat_r)
libc.a(lib_a-fvwrite.o)       stubs.o (__sfvwrite_r)

Allocating common symbols
Common symbol       size              file

errno               0x4               libc.a(lib_a-reent.o)
__mib_reserved__stack
                    0x8000            stubs.o

Memory Configuration

Name             Origin             Length             Attributes
EXTERNAL_DRAM_0  0x000000008e000000 0x0000000001000000 axwl
EXTERNAL_DRAM_1  0x000000008f000000 0x0000000001000000 axwl
IVT_RAM          0x0000000000000000 0x0000000000000028 axwl
WORKGROUP_RAM    0x0000000000000028 0x0000000000000030 axwl
INTERNAL_RAM     0x0000000000000058 0x0000000000007fa8 axwl
BANK0_SRAM       0x0000000000000058 0x0000000000001fa8 axwl
BANK1_SRAM       0x0000000000002000 0x0000000000002000 axwl
BANK2_SRAM       0x0000000000004000 0x0000000000002000 axwl
BANK3_SRAM       0x0000000000006000 0x0000000000002000 axwl
*default*        0x0000000000000000 0xffffffffffffffff

Linker script and memory map

LOAD /home/mib/ephiphany-sdk/esdk/tools/e-gnu.armv7l/epiphany-elf/lib/crt0.o
LOAD /home/mib/ephiphany-sdk/esdk/tools/e-gnu.armv7l/lib/gcc/epiphany-elf/4.8.2/crti.o

os_A            0x0000000000000000        0x8
 *.o(pattern_A_empty)
 *.o(pattern_A)
 is_A1          0x0000000000000000        0x8 /a1/input/section.o
                0x0000000000000000                .symbol_a1_1
                0x0000000000000003                .symbol_a1_2

os_B            0x0000000000000006        0x8
 *.o(pattern_B)
 is_B1
                0x0000000000000006        0x8 /b1/input/section.o

os_C            0x0000000000000004        0xc
 *.o(pattern_C)
 is_C1          0x0000000000000004        0xc /b1/input/section.o
"""

#  0 1 2 3 4 5 6 7 8 9 a b c d e f
#  ----- os_A ----
#          ---------- os_C -------
#              ------ os_B ---

import unittest
from ldmap import LinkerMap
from collections import Counter, defaultdict
import group

class RangeTest(unittest.TestCase):
    def setUp(self):
        self.c = [x+'\n' for x in ldf.split('\n')]

    def test_os_lookup(self):
        M = LinkerMap(iter(self.c))
        self.assertFalse(M.error_log)
        self.assertEqual(len(M.input_sections), 3)
        self.assertEqual(len(M.output_sections), 3)
        oa, ob, oc = M.output_sections
        self.assertEqual([oa.name, ob.name, oc.name],
            ['os_A', 'os_B', 'os_C'])
        self.assertEqual(M.get_output_sections(0), [oa,])
        self.assertEqual(M.get_output_sections(3), [oa,])
        self.assertEqual(M.get_output_sections(4), [oa,oc])
        self.assertEqual(M.get_output_sections(5), [oa,oc])
        self.assertEqual(M.get_output_sections(6), [oa,ob,oc])
        self.assertEqual(M.get_output_sections(7), [oa,ob,oc])
        self.assertEqual(M.get_output_sections(8), [ob,oc])
        self.assertEqual(M.get_output_sections(0xd), [ob,oc])
        self.assertEqual(M.get_output_sections(0xe), [oc])
        self.assertEqual(M.get_output_sections(0xf), [oc])


class GreedyGrouperTest(unittest.TestCase):
    def test_constructor(self):
        s = {
            'a' : 1,
            'b' : 1,
            'c' : 1,
            'd' : 1,
            'e' : 1,
            'f' : 1,
        }
        e = {
            ('a', 'b') : 100,
            ('c', 'd') : 100,
            ('e', 'f') : 100,
            

            ('a', 'c') : 3,
            ('b', 'd') : 0,

            ('a', 'e') : 2,
            ('b', 'e') : 2,
        }
        g = group.GreedyGrouper([], s, 4)
        self.assertEqual(g.groupFromG(Counter(e), s.keys()),
            set([('e', 'f'), ('a', 'b', 'c', 'd')]))


class GreedyGrouper2Test(unittest.TestCase):
    def test_t1(self):
        s = {
            'a' : 1,
            'b' : 1,
            'c' : 1,
            'd' : 1,
            'e' : 1,
            'f' : 1,
        }
        e = {
            ('a', 'b') : 100,
            ('c', 'd') : 100,
            ('e', 'f') : 100,
            

            ('a', 'c') : 3,
            ('b', 'd') : 0,

            ('a', 'e') : 2,
            ('b', 'e') : 2,
        }        
        g = group.GreedyGrouper2([], s, 4)
        self.assertEqual(g.groupFromG(Counter(e), s.keys()),
            set([('c', 'd'), ('a', 'b', 'e', 'f')]))


    def test_t2(self):
        s = {
            'a' : 1,
            'b' : 1,
            'c' : 1,
            'd' : 1,
        }
        e = {
            ('a', 'b') : 1,
            ('b', 'c') : 2,
            ('c', 'd') : 1,
        }        
        g = group.GreedyGrouper2([], s, 2)
        self.assertEqual(g.groupFromG(Counter(e), s.keys()),
            set([('a',), ('b', 'c'), ('d',)]))


class RatioGrouperTest(unittest.TestCase):
    def test_t1(self):
        s = {
            'a' : 1,
            'b' : 1,
            'c' : 1,
            'd' : 1,
            'e' : 1,
            'f' : 1,
        }
        e = {
            ('a', 'b') : 100,
            ('c', 'd') : 100,
            ('e', 'f') : 100,
            

            ('a', 'c') : 3,
            ('b', 'd') : 0,

            ('a', 'e') : 2,
            ('b', 'e') : 2,
        }        
        g = group.RatioGrouper([], s, 4)
        self.assertEqual(g.groupFromG(Counter(e), s.keys()),
            set([('e', 'f'), ('a', 'b', 'c', 'd')]))


    def test_t2(self):
        s = {
            'a' : 1,
            'b' : 1,
            'c' : 1,
            'd' : 1,
        }
        e = {
            ('a', 'b') : 1,
            ('b', 'c') : 2,
            ('c', 'd') : 1,
        }        
        g = group.RatioGrouper([], s, 2)
        self.assertEqual(g.groupFromG(Counter(e), s.keys()),
            set([('a', 'b'), ('c', 'd')]))

class StageGrouper(unittest.TestCase):
    def test_simple(self):
        order = [
            'main',
            'f',
            'main',
            'g',
            'main',
            ]
        s = {
            'main': 1,
            'f': 0,
            'g': 0,
        }
        sg = group.StageGrouper(order, s, 1)
        intervals = sg.make_intervals(order)
        self.assertEqual(intervals,
            {'main': (1, 5), 'f': (2, 2), 'g': (4, 4)})
        events = sg.make_events(intervals)
        Event = group.StageGrouper.Event
        self.assertEqual(events,[
            Event(x=2, type='START', interval=(2, 11), label='main'), 
            Event(x=4, type='START', interval=(4, 5), label='f'), 
            Event(x=5, type='END', interval=(4, 5), label='f'), 
            Event(x=8, type='START', interval=(8, 9), label='g'), 
            Event(x=9, type='END', interval=(8, 9), label='g'), 
            Event(x=11, type='END', interval=(2, 11), label='main')])

        self.assertEqual(sg.solve(0, 5, 1, events), (1, set()))
        self.assertEqual(sg.solve(4, 5, 1, events), (1, set()))
        self.assertEqual(sg.solve(8, 9, 1, events), (1, set()))
        self.assertEqual(sg.solve(8, 10, 1, events), (1, set()))
        self.assertEqual(sg.solve(2, 11, 1, events), (2, {'main'}))                

        self.assertEqual(sg.group(),
            {('f',), ('g',)})

    def test_simple2(self):
        order = [
            'f',
            'g',
            ]
        s = {
            'main': 1,
            'f': 0,
            'g': 0,
        }
        sg = group.StageGrouper(order, s, 100)
        self.assertEqual(sg.group(),
            {('f',), ('g',)})

class SingleOverlayGrouper(unittest.TestCase):
    def test_simple(self):
        order = [
            'main',
            'f',
            'main',
            'g',
            'main',
            ]
        s = {
            'main': 1,
            'f': 2,
            'g': 3,
        }
        i_freq = {
            'main': 12,
            'f' : 1,
            'g' : 3
        }
        gg = group.SingleOverlayGrouper(order, s, 3, instructionCounts=i_freq)
        
        self.assertEqual(gg.group(),
            {('f', 'main')})

class LinkGrouperTest(unittest.TestCase):
    def test_simple(self):
        order = ['a',
            'f', 'g', 'f',
            'f', 'g', 'f',
            'f', 'h', 'f',
            'g', 'h', 'g',
            'a', 'b', 'a',
            'b', 'a', 'b',
        ]
        s = defaultdict(lambda : 1)
        ic = dict( [(x, 1) for x in order] )
        lg = group.LinkGrouper(order, s, max_size_total=4, always_internal_size=1, instructionCounts=ic)
        self.assertEqual(lg.prefixSums([3, 2, 1, 4, 5]),
            [0, 3, 5, 6, 10, 15])
        self.assertEqual(lg.group(),
            set([('a', 'b'), ('g', 'h')])
        )
        self.assertEqual(lg.get_internal(), ('f',))

        lg = group.LinkGrouper(order, s, max_size_total=100, always_internal_size=1, instructionCounts=ic)


def main():
    unittest.main()

if __name__ == '__main__':
    main()
