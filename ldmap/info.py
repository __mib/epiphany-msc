import sys
from ldmap import LinkerMap

m = LinkerMap(open(sys.argv[1]))

print "Symbols"
for x in m.symbols:
    print x

print "Regions"
for x in m.regions:
    print x

print "Input sections"
for x in m.input_sections:
    print x

print "Output sections"
for x in m.output_sections:
    print x

print "Input LOOKUP"
for x in m.input_section_lookup:
    print x

print "Output LOOKUP"
for x in m.output_section_lookup:
    print x

print "Error log"
for x, y in m.error_log:
    print "Line: ", x.strip()
    print y
    print ""

for x in m.output_sections:
    if x.origin == None or x.length == None:
        print x
