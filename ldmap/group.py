"""
Groups sections into overlays.
group.py linker_map max_size gefx_filename algo trace_part1 trace_part2 ...

Input line format:
count(ignored) label 
"""

import os, sys, itertools, fileinput, heapq, json
from collections import defaultdict, Counter, namedtuple
from ldmap import LinkerMap, pairwise, humanized


def is_valid_label(label):
    return label.startswith(".text") and label != ".text" # We could check if the derived symbol name is actually present in symtab


def safe_get_isection_size(name):
    L = M.get_isection_for_name(i)
    if len(L) != 1:
        print >> sys.stderr, "ISection:", name, "lookup returned:", L, "\n"
    return L[0].length



"""
Each functions lands in a separate overlay.
"""
class TrivialGrouper(object):
    def __init__(self, order, sizes=None, max_size=0):
        self.labels = set(order)

    def group(self):
        return [(x,) for x in self.labels]

"""
Processes pairs of labels. Pairs that most commonly go together are processed
first and their ovelays are merged if the size constrait is still satisfied.
"""
class GreedyGrouper(object):
    def __init__(self, order, sizes, max_size):
        self.order = order
        self.sizes = sizes
        self.max_size = max_size

    def group(self):
        edges = Counter( [ tuple(sorted(p)) for p in pairwise(self.order) ] )
        return self.groupFromG(edges, self.order)

    def groupFromG(self, edges, labels):
        group = dict((a, [a,]) for a in set(labels))

        def group_size(g):
            s = 0
            for x in g:
                s += self.sizes[x]
            return s

        for ((a, b), cnt) in edges.most_common():
            if group_size(group[a]) + group_size(group[b]) <= self.max_size and \
               group[a] != group[b]:
                group[a] += group[b]
                for x in list(group[b]):
                    group[x] = group[a]

        return set(map(tuple, group.values()))

"""
Similar to GreedyGrouper, but processes pairs of overlays.
"""
class GreedyGrouper2(object):
    def __init__(self, order, sizes, max_size):
        self.order = order
        self.sizes = sizes
        self.M = max_size

    def rate(self, a, b):
        self.threshold = 0

        if sum( [ self.sizes[x] for x in a ] ) + \
            sum( [ self.sizes[x] for x in b ] ) > self.M:
                return -1

        r = 0
        for f in a:
            for g in b:
                r += self.edges[ tuple(sorted((f, g))) ]
        return r

    def merge(self, a, b):
        pass

    def get_best_pair(self, overlays):
        best = None
        for a in overlays:
            for b in overlays:
                if a == b:
                    continue
                if not best or self.rate(a, b) > self.rate(*best):
                    best = (a, b)

        if best and self.rate(*best) > self.threshold:
            return best
        return None

    def group(self):
        labels = set(self.order)
        edges = Counter( [ tuple(sorted(p)) for p in pairwise(self.order) ] )
        return self.groupFromG(edges, labels)

    def groupFromG(self, edges, labels):
        overlays = set( [ frozenset([x]) for x in labels ] )
        self.edges = edges

        while True:
            p = self.get_best_pair(overlays)
            if not p:
                break
            self.merge(*p)
            a, b = p
            overlays.remove(a)
            overlays.remove(b)
            overlays.add(a | b)

        return set( tuple(sorted(x)) for x in overlays )

class RatioGrouper(GreedyGrouper2):
    def rate(self, a, b):
        self.threshold = (0.9, 0)

        if sum( [ self.sizes[x] for x in a ] ) + \
            sum( [ self.sizes[x] for x in b ] ) > self.M:
                return (-1, 0)

        r = 0
        for f in a:
            for g in b:
                r += self.edges[ tuple(sorted((f, g))) ]
        if r == 0:
            return (0, 0)
        wa = self.weight[a] * 1.0
        wb = self.weight[b] * 1.0
        score = max(r / wa, r / wb)
        return (score, r)

    def merge(self, a, b):
        o = a | b
        for ((f, g), c) in self.edges.iteritems():
            if f != g and ((f in o) != (g in o)):
                self.weight[o] += c

    def groupFromG(self, edges, labels):
        self.weight = defaultdict(lambda : 0)
        for ((a, b), c) in edges.iteritems():
            if a != b:
                self.weight[frozenset([a])] += c
                self.weight[frozenset([b])] += c
        return super(RatioGrouper, self).groupFromG(edges, labels)

"""
Creates a single overlay with sections containing most instructions.
TODO: dp
"""
class SingleOverlayGrouper(object):
    def __init__(self, order, sizes, max_size, instructionCounts):
        self.order = order
        self.sizes = sizes
        self.max_size = max_size
        self.instructionCounts = instructionCounts

    def group(self):
        order = sorted(self.instructionCounts, key=self.instructionCounts.get)[::-1]
        total_size = 0
        group = []
        for section in order:
            if total_size + self.sizes[section] <= self.max_size:
                group.append(section)
                total_size += self.sizes[section]
        group = sorted(group)
        group = tuple(group)
        return {group}

"""
max_size limits the total size of discarded sections
"""
class StageGrouper(object):
    def __init__(self, order, sizes, max_common_size, dump=None, downsample=1):
        self.order = order
        self.sizes = sizes
        self.M = max_common_size
        self.cache = {}
        self.dump = dump
        self.downsample = downsample

    Event = namedtuple(
        'Event', ['x', 'type', 'interval', 'label'])

    def make_intervals(self, order):
        first = {}
        last = {}
        i = 0
        for x in order:
            i += 1
            if x not in first:
                first[x] = i
            last[x] = i
        r = {}
        for x in first:
            r[x] = (first[x], last[x])
        return r

    def solve(self, a, b, k, events):
        if (a, b, int(k / self.downsample)) not in self.cache:
            max_score = 0
            best_cuts = set()
            current_cut = set()
            current_cut_weight = 0
            active_events = 0
            for e in events:
                if a <= e.interval[0] and e.interval[1] <= b:
                    active_events += 1
                    if e.type == 'START':
                        current_cut_weight += self.sizes[e.label]
                        current_cut.add(e.label)
                    else:
                        current_cut_weight -= self.sizes[e.label]
                        current_cut.remove(e.label)

                    if k < current_cut_weight or e.x == b:
                        continue

                    left_score, left_cuts = self.solve(a, e.x, k-current_cut_weight, events)
                    right_score, right_cuts = self.solve(e.x+1, b, k-current_cut_weight, events)
                    if left_score + right_score > max_score:
                        max_score = left_score + right_score
                        best_cuts = current_cut | left_cuts | right_cuts
            if active_events > 0 and max_score == 0:
                max_score = 1
                best_cuts = set()

            self.cache[ (a, b, int(k / self.downsample)) ] = (max_score, best_cuts)

        return self.cache[ (a, b, int(k / self.downsample)) ]

    def make_events(self, intervals):
        events = []
        for key in intervals:
            a, b = intervals[key]
            events.append( StageGrouper.Event(x=2*a, type='START', interval=(2*a, 2*b+1), label=key) )
            events.append( StageGrouper.Event(x=2*b+1, type='END', interval=(2*a, 2*b+1), label=key) )

        return sorted(events, key=lambda e: e.x)

    def groupIE(self, intervals, events):
        stage_cnt, cut = self.solve(1, 2 * len(self.order) + 10, self.M, events)
        stages = []
        for (key, (a, b)) in intervals.iteritems():
            if key not in cut:
                stages.append( (a, b, key) )
        stages = sorted(stages)
        max_end = 0
        groups = set()
        current_group = []
        for (a, b, name) in stages:
            if a > max_end:
                groups.add( tuple(sorted(current_group)) )
                current_group = []
            current_group.append(name)
            max_end = max(max_end, b)
        groups.add( tuple(sorted(current_group)) )
        return {g for g in groups if g}

    def group(self):
        intervals = self.make_intervals(self.order)
        events = self.make_events(intervals)
        if self.dump:
            f = open(self.dump, 'w')
            print >> f, json.dumps( (intervals, events) )
            f.close()
        return self.groupIE(intervals, events)


class LinkGrouper(object):
    def __init__(self, order, sizes, max_size_total=6*1024, always_internal_size=6*1024, instructionCounts=None):
        self.order = order
        self.sizes = sizes
        self.max_size_total = max_size_total
        self.always_internal_size = always_internal_size
        self.instructionCounts = instructionCounts

    def prefixSums(self, T):
        result = [0]
        currentSum = 0
        for x in T:
            currentSum += x
            result.append(currentSum)
        return result

    def intervalSum(self, prefixSums, a, b):
        assert a > 0
        return prefixSums[b] - prefixSums[a-1]

    def get_internal(self):
        return self.internal

    def group(self):
        order = self.order

        sg = SingleOverlayGrouper(order, self.sizes, self.always_internal_size, 
            instructionCounts=self.instructionCounts)
        internal = list( sg.group())[0]
        self.internal = internal

        previousOccurenceIndex = dict()
        prefixSums = self.prefixSums([self.sizes[x] * (x not in internal) for x in order])
        currentIndex = 1
        groupCounter = Counter()

        for label in order:
            if label in previousOccurenceIndex and label not in internal:
                weight = self.intervalSum(prefixSums, previousOccurenceIndex[label], currentIndex)
                if 0 < weight <= self.max_size_total - self.always_internal_size:
                    fset = frozenset(
                        [x for x in order[previousOccurenceIndex[label]-1 : currentIndex]
                            if x not in internal]
                    )
                    if len(fset) > 1:
                        groupCounter.update([fset,])

            previousOccurenceIndex[label] = currentIndex
            currentIndex += 1
        """
        for x in groupCounter.keys():
            xIsASubset = False
            for y in groupCounter:
                if x.issubset(y) and x != y and groupCounter[y] >= 0.9 * groupCounter[x]:
                    groupCounter[y] += groupCounter[x]
                    xIsASubset = True
            if xIsASubset:
                del groupCounter[x]
        """

        visited = set()
        result = set()
        for (x, _) in groupCounter.most_common():
            overlapped = False
            for y in x:
                if y in visited:
                    overlapped = True
            if not overlapped:
                visited = visited | x
                result.add(tuple(sorted(list(x))))
        return result


def check_grouping(G, internal, max_size, size, labels):
    seen = set(internal)
    for group in G:
        for x in group:
            if x in seen: raise Exception("label occurs multiple times")
            seen.add(x)
        total_size = sum([size[x] for x in group])
    if seen != labels:
        print >> sys.stderr, "Not all labels were used."
        return labels - seen
    return set()



def grade_grouping(G, order, size):
    print >> sys.stderr, "Overlay count:", len(G)
    label2overlay = dict()
    label2gs = dict()

    for g in G:
        gs = 0
        for l in g:
            label2overlay[l] = g
            gs += size[l]
        for l in g:
            label2gs[l] = int(gs)

    same = 0
    for (a, b) in pairwise(order):
        if label2overlay[a] == label2overlay[b]:
            same += 1

    print >> sys.stderr, "Times overlays will be reloaded", humanized( len(order) - same ), \
        "({:.2f}%)".format(100.0 * (len(order) - same) / len(order))
    print >> sys.stderr, "Max overlay size:", humanized(max(label2gs.values()))
    print >> sys.stderr, "Total size of all reloads:", humanized( sum( 
        [ label2gs[b] for (a, b) in pairwise(order) if label2overlay[a] != label2overlay[b] ] ) )



def label2symbol(label):
    if label == '.text.startup.main':
        return '_main'
    label = label[len('.text')+1:]
    return "_" + label

def output_map_cfg(G, internal=[]):
    gid = 0
    for g in G:
        gid += 1
        group_name = "ovly_group_" + str(gid)
        for label in g:
            name = label2symbol(label)
            print name, "__mib_stub_" + name, group_name
    for section in internal:
        print label2symbol(section), "_", "-INTERNAL"


def gv_debug(G, edges, size):
    from xml.etree.ElementTree import Element, SubElement, tostring

    i = 0

    gexf = Element('gexf', {
     #   'xmlns': "http://www.gexf.net/1.2draft",
     #   'xmlns:viz': "http://www.gexf.net/1.1draft/viz",
     #   'xsi:schemaLocation': "http://www.gexf.net/1.2draft http://www.gexf.net/1.2draft/gexf.xsd",
     #   'version': "1.2"
        })

    graph = SubElement(gexf, 'graph', {
        #'mode': 'static',
        'defaultedgetype': 'undirected'
        })

    attrs = SubElement(graph, 'attributes', {'class': 'node'})
    size_attr = SubElement(attrs, 'attribute', {
        'id': '0',
        'title': 'size',
        'type': 'integer'
        })
    instr_cnt = SubElement(attrs, 'attribute', {
        'id': '1',
        'title': 'instr_cnt',
        'type': 'integer'
        })

    nodesn = SubElement(graph, 'nodes')
    edgesn = SubElement(graph, 'edges')

    for group in G:
        i += 1
        total_weight = sum( [size[label] for label in group] )
        group_node = SubElement(nodesn, 'node', {
            'id': str(i),
            'label': "Overlay_" + str(i)
            })
        attrs = SubElement(group_node, 'attvalues')
        size_attr = SubElement(attrs, 'attvalue', {
            'for': '0',
            'value': str(total_weight)
            })

        for label in group:
            node = SubElement(group_node, 'node', {
                'id' : label,
                'label' : label
                })
            attrs = SubElement(node, 'attvalues')
            size_attr = SubElement(attrs, 'attvalue', {
                'for': '0',
                'value': str(size[label])
                })
            size_attr = SubElement(attrs, 'attvalue', {
                'for': '1',
                'value': str(label2inst_cnt[label])
                })

    for ((a, b), cnt) in edges.most_common():
        edge = SubElement(edgesn, 'edge', {
            'source': a,
            'target': b,
            'weight': str(cnt) 
            })
    
    f = open(gefx_filename, 'w')
    print >> f, '<?xml version="1.0" encoding="UTF-8"?>'
    print >> f, tostring(gexf)
    f.close()


if __name__ == '__main__':
    M = LinkerMap(open(sys.argv[1]))
    max_size = int(sys.argv[2])
    gefx_filename = sys.argv[3]
    algo_name = sys.argv[4]
    del sys.argv[1:5]

    execution_order = []

    invalid_labels = set()
    invalid_label_cnts = 0
    total_cnts = 0
    label2inst_cnt = defaultdict(lambda : 0)

    for line in fileinput.input():
        cnt, label = line.strip().split()
        total_cnts += int(cnt)
        if is_valid_label(label): # This will make holes in the execution order, FIXME.
            execution_order.append(label)
            label2inst_cnt[label] += int(cnt)
        else:
            invalid_label_cnts += int(cnt)
            invalid_labels.add(label)

    labels = set(execution_order)
    size = dict([(i, safe_get_isection_size(i)) for i in labels])

    print >> sys.stderr, "Execution log length:", humanized( len(execution_order) )
    print >> sys.stderr, "Section count:", len(labels)
    print >> sys.stderr, "Max section size:", humanized(max(size.values()))
    print >> sys.stderr, "Total section size:", humanized(sum(size.values()))
    print >> sys.stderr, "Total instruction count:", humanized(total_cnts)
    print >> sys.stderr, "Invalid labels total instruction count:", humanized(invalid_label_cnts), \
        "({:.2f}%)".format(100.0 * invalid_label_cnts / total_cnts)
    print >> sys.stderr, "Invalid labels:", invalid_labels

    algos = {
        'greedy': GreedyGrouper,
        'trivial': TrivialGrouper,
        'greedy2': GreedyGrouper2,
        'ratio': RatioGrouper,
        'stage': StageGrouper,
        'single': SingleOverlayGrouper,
        'link': LinkGrouper
    }

    algo = algos[algo_name]

    if algo_name == 'stage':
        obj = algo(execution_order, size, max_size, dump=None, downsample=10)
    elif algo_name in ['single', 'link']:
        obj = algo(execution_order, size, max_size, instructionCounts=label2inst_cnt)
    else:
        obj = algo(execution_order, size, max_size)



    result = list( obj.group() )
    internal = []
    if hasattr(obj, 'get_internal'):
        internal = obj.get_internal()


    not_grouped = check_grouping(result, internal, max_size, size, labels)

    if algo_name == 'stage':
        internal = not_grouped
        not_grouped = check_grouping(result, internal, max_size, size, labels)

    slow_ops = 0
    for x in not_grouped:
        slow_ops += label2inst_cnt[x]
    print >> sys.stderr, "Slow instruction count:", humanized(slow_ops)



    print >> sys.stderr, "Not grouped ", len(not_grouped), " labels of total size: ", \
        humanized(sum([size[x] for x in not_grouped])), not_grouped




    grade_grouping(result, [x for x in execution_order if x not in (set(not_grouped) | set(internal))], size)

    gv_debug( result,
        Counter( [ tuple(sorted(p)) for p in pairwise(execution_order) ] ),
        size )
    output_map_cfg(result, internal=internal)
