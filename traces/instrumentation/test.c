#include <stdio.h>

#define MAXT 16*1024
extern char MT_OP[MAXT];
extern void* MT_AD[MAXT];
extern int entries;
int e;

int f(int a) {
    return a + 10;
}

int main() {
    printf("zaczynamy");
    int a = 12;
    a = f(a);

    e = entries;
    int i;
    for(i=0; i<e; ++i) {
        printf("%s %p\n", MT_OP[i] == 'E' ? "ENTER" : "EXIT", MT_AD[i]);
    }

    return 0;
}
