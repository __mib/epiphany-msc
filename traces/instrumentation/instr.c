#include <stdio.h>

FILE* tFile = 0;
char enable = 1;

void __cyg_profile_func_enter(void *this_fn, void *call_site) {
    if (enable) {
        enable = 0;
        if (! tFile) tFile = fopen("trace.mib", "w");
        fprintf(tFile, "ENTER %p\n", this_fn);
        fflush(tFile);
        enable = 1;
    }
}

void __cyg_profile_func_exit(void *this_fn, void *call_site) {
    if (enable) {

        enable = 0;
        if (! tFile) tFile = fopen("trace.mib", "w");
        fprintf(tFile, "EXIT %p\n", this_fn);
        fflush(tFile);
        enable = 1;
    }
}

