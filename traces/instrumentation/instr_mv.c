#include <stdio.h>

FILE* tFile = 0;
char enable = 1;
int entries = 0;
#define MAXT 16*1024
char MT_OP[MAXT];
void* MT_AD[MAXT];

void __cyg_profile_func_enter(void *this_fn, void *call_site) {
    MT_OP[entries] = 'E';
    MT_AD[entries] = this_fn;
    ++entries;
}

void __cyg_profile_func_exit(void *this_fn, void *call_site) {
    MT_OP[entries] = 'X';
    MT_AD[entries] = this_fn;
    ++entries;
}

