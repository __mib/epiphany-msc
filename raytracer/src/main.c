//
//  main.c
//  raytracer
//
//  Created by Krzysztof Gabis on 13.04.2013.
//  Copyright (c) 2013 Krzysztof Gabis. All rights reserved.
//

#include <stdio.h>
#include <string.h>

#include "raytracer.h"
#include "csfml_render.h"
#include "png_render.h"

#include "utils.h"

#define WINDOW_WIDTH 5 // 10 //720
#define WINDOW_HEIGHT 5 //5 //640

void selectDemo(int argc, const char *argv[], Scene *scene) {
    if (argc != 2) {
        scene_loadSpheresDemo(scene);
    } else if (STREQ(argv[1], "spheres")) {
        scene_loadSpheresDemo(scene);
    } else if (STREQ(argv[1], "snowman")) {
        scene_loadSnowmanDemo(scene);
    } else if (STREQ(argv[1], "teapot")) {
        scene_loadTeapotDemo(scene);
    }
}

int main_original(int argc, const char *argv[]) {
    Raytracer rt;
    raytracer_init(&rt, WINDOW_WIDTH, WINDOW_HEIGHT);
    selectDemo(argc, argv, &rt.scene);    
    int result = png_render(&rt);
    raytracer_dealloc(&rt);

    return result;
}

const char tmp[] = "spheres\0";

int Main(int arg) {
    const char* args[] = {0, tmp};
    return main_original(2, args);
}
