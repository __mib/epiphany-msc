    .section    .text.mib_stubs.{stub_name},"ax",@progbits
    .balign 4
    .global {stub_name}

{stub_name}:
    mov  r16, %low (__load_start_{overlay_section_name})
    movt r16, %high(__load_start_{overlay_section_name})

    mov  r17, %low (__load_stop_{overlay_section_name})
    movt r17, %high(__load_stop_{overlay_section_name})

    mov r18, %low({orig_name})
    movt r18, %high({orig_name})
    
    mov r44, %low(mib_overlay_handler)
    movt r44, %high(mib_overlay_handler)
    jr r44

    .size   {stub_name}, .-{stub_name}