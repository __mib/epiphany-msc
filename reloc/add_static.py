#!/usr/bin/python2.7
import sys, os

NOT_AN_OVERLAY = ['-', '-INTERNAL']

if __name__ == "__main__":
    f = open(sys.argv[1])
    for (a, b, c) in [x.split() for x in f]:
        print a, b, c
        if c not in NOT_AN_OVERLAY:
            if a.startswith('_'):
                print ".text." + a[1:], b, "-"
    f.close()
