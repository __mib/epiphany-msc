    .comm   ___mib_reserved__stack,32768,8 // TODO: do we need any safety mechanism for overflows?
//  .comm   ___mib_reserved__stack,10000000,8 // TODO: do we need any safety mechanism for overflows?

    .global ___mib_reserved__sp
    //.section .data
    .section .mib.codata
    .balign 4
    .type   ___mib_reserved__sp, @object
    .size   ___mib_reserved__sp, 4
___mib_reserved__sp: // stack pointer ...
    .word   ___mib_reserved__stack // ... initialized
    
    .global ___mib_reserved__co_start // current overlay
    //.section .bss // FIXME: -> INTERNAL_RAM, hard-coded locoation
    .section .mib.codata
    .balign 4
    .type   ___mib_reserved__co_start, @object
    .size   ___mib_reserved__co_start, 4
___mib_reserved__co_start:
    .word 16 // dummy value, SHOULD NOT MATCH THE START OF THE FIRST LOADED OVERLAY
    ;.zero  4
    
    .global ___mib_reserved__co_stop // current overlay
    .balign 4
    .type   ___mib_reserved__co_stop, @object
    .size   ___mib_reserved__co_stop, 4
___mib_reserved__co_stop:
    .word 16 // dummy value
    ;.zero  4

    .global mib_dma_descriptor // current overlay
    .balign 4
    .type   mib_dma_descriptor, @object
    .size   mib_dma_descriptor, 24

mib_dma_descriptor:
mib_dma_descriptor_wtf:
    .word 0x00000463 // DMAxCONFIG
    .word 0x00080008 // INNER STRIDE
mib_dma_count:
    .word 0xdeadbeef
    .word 0x00000000 // OUTER STRIDE
mib_dma_source:
    .word 0xdeadbeef
mib_dma_dst:
    .word 0xdeadbeef


// Handles actual overlay loading
// Arguments:
// - r16  ==  __load_start
// - r17  ==  __load_stop
// - r18  ==  original function virtual address


    .section    .text.mib_overlay_handler,"ax",@progbits
    .balign 4
    .global mib_overlay_handler

mib_overlay_handler:
    // FAST PATH

    mov  r49, %low (___mib_reserved__co_start)
    movt r49, %high(___mib_reserved__co_start)
    ldr r44, [r49] // r44 :=  __load_start    

    // mov  r45, %low (__load_start_{overlay_section_name}) // Load new START
    // movt r45, %high(__load_start_{overlay_section_name})
    // sub r49, r44, r45 // compare r44 == r45
    sub r49, r44, r16
    beq mib_overlay_handler_fastpath

    // ---- no state is saved ----

    // NORMAL PATH

    mov  r47, %low (___mib_reserved__sp) // load MSP
    movt r47, %high(___mib_reserved__sp)
    ldr r48, [r47] // r48 := MSP
    str lr, [r48] // save LR to mibstack
    add r48,r48,#4
    
    // Update START address
    mov  r49, %low (___mib_reserved__co_start)
    movt r49, %high(___mib_reserved__co_start)
    ldr r44, [r49] // Load old START
    str r44, [r48] // Push old START to MSP
    add r48,r48,#4 // move local MSP
    // mov  r44, %low (__load_start_{overlay_section_name}) // Load new START
    // movt r44, %high(__load_start_{overlay_section_name})
    mov r44, r16

    str r44, [r49] // Update __co_start to new START

    // Update STOP address
    mov  r49, %low (___mib_reserved__co_stop)
    movt r49, %high(___mib_reserved__co_stop)
    ldr r45, [r49] // Load old STOP
    str r45, [r48] // Push old STOP to MSP
    add r48,r48,#4
    // mov  r45, %low (__load_stop_{overlay_section_name}) // Load new STOP
    // movt r45, %high(__load_stop_{overlay_section_name})
    mov r45, r17
    str r45, [r49] // Update __co_stop to new STOP

    str r48, [r47] // save MSP

    // Load destination
    mov  r46, %low (__mib_overlay_vma)
    movt r46, %high(__mib_overlay_vma)

    // Copy the overlay
    // Copies range [r44, r45) to [r46, ...)

    // DMA count setup

    mov  r21, %low (mib_dma_count)
    movt r21, %high(mib_dma_count)

    sub r20, r45, r44
    lsr r20, r20, #3
    movt r20, #1

    str r20, [r21]

    // DMA source setup

    mov  r21, %low (mib_dma_source)
    movt r21, %high(mib_dma_source)

    str r44, [r21]

    // DMA dst setup

    mov  r21, %low (mib_dma_dst)
    movt r21, %high(mib_dma_dst)

    str r46, [r21]

    // start transfer

    mov  r20, %low(mib_dma_descriptor_wtf)
    movt r20, %high (mib_dma_descriptor_wtf)

    lsl r22, r20, #16
    mov  r20, 0x8
    orr r21, r20, r22

    movts DMA0CONFIG, r21

    // wait for the transfer to finish

    movt r20, #0
    mov r20, #15 // dma status mask

mib_overlay_handler_load_next:
    movfs r22, DMA0STATUS
    and r21, r20, r22 // If ( RD[31:0] == 0 ) { AZ=1 }
    bne mib_overlay_handler_load_next

/*    
    sub r47, r45, r44 // r47 = r45 - r44, r45 == co_stop, r44 == current position
    beq mib_overlay_handler_load_end

    ldr r20, [r44]
    str r20, [r46]
    add r44, r44, #4
    add r46, r46, #4

    b mib_overlay_handler_load_next
*/

mib_overlay_handler_load_end:

    // Jump to original target

    // mov r44, %low({orig_name})
    // movt r44, %high({orig_name}) // TODO: change jarl to bl?
    mov r44, r18

    // NO STATE IS SAVED ---------------------------
    jalr r44
    // NO STATE IS SAVED ---------------------------

    // Restore the original overlay

    mov  r47, %low (___mib_reserved__sp) // load MSP
    movt r47, %high(___mib_reserved__sp)
    ldr r48, [r47] // r48 becomes MSP
    sub r48,r48,#4

    // Update STOP address
    mov  r49, %low (___mib_reserved__co_stop)
    movt r49, %high(___mib_reserved__co_stop)
    ldr r45, [r48] // Load old STOP from MSP
    sub r48,r48,#4
    str r45, [r49] // Update __co_stop
 
    // Update START address
    mov  r49, %low (___mib_reserved__co_start)
    movt r49, %high(___mib_reserved__co_start)
    ldr r44, [r48] // Load old STOP from MSP
    sub r48,r48,#4
    str r44, [r49] // Update __co_start

    ldr lr, [r48]   // load LR from mibstack
    str r48, [r47]  // save MSP

    // Load destination
    mov  r46, %low (__mib_overlay_vma)
    movt r46, %high(__mib_overlay_vma)

    // Copy the overlay


    mov  r21, %low (mib_dma_count)
    movt r21, %high(mib_dma_count)

    sub r20, r45, r44
    beq mib_overlay_handler_x_load_end
    lsr r20, r20, #3
    movt r20, #1

    str r20, [r21]

    // DMA source setup

    mov  r21, %low (mib_dma_source)
    movt r21, %high(mib_dma_source)

    str r44, [r21]

    // DMA dst setup

    mov  r21, %low (mib_dma_dst)
    movt r21, %high(mib_dma_dst)

    str r46, [r21]

    // start transfer

    mov  r20, %low(mib_dma_descriptor_wtf)
    movt r20, %high (mib_dma_descriptor_wtf)

    lsl r22, r20, #16
    mov  r20, 0x8
    orr r21, r20, r22

    movts DMA0CONFIG, r21

    // wait for the transfer to finish

    movt r20, #0
    mov r20, #15 // dma status mask

mib_overlay_handler_x_load_next:
    movfs r22, DMA0STATUS
    and r21, r20, r22 // If ( RD[31:0] == 0 ) { AZ=1 }
    bne mib_overlay_handler_x_load_next



/*
mib_overlay_handler_x_load_next:
    
    sub r47, r45, r44
    beq mib_overlay_handler_x_load_end

    ldr r20, [r44]
    str r20, [r46]
    add r44, r44, #4
    add r46, r46, #4

    b mib_overlay_handler_x_load_next
*/


mib_overlay_handler_x_load_end:
    
    rts

mib_overlay_handler_fastpath:
    // we jump to the original function without changing the link register,
    // that way we will not return to unload this overlay
    // mov r44, %low({orig_name})
    // movt r44, %high({orig_name})
    mov r44, r18
    jr r44

    .size   mib_overlay_handler, .-mib_overlay_handler

