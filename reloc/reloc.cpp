#include "config.h"
#include "bfd.h"

#include <malloc.h>
#include <iostream>
#include <cstdlib>
#include <cassert>
#include <map>
#include <set>
#include <fstream>
using namespace std;

map<string, string> symbol2Stub;
map<asymbol**, asymbol**> sym_ptr2Stub_ptr;

void* pmalloc(size_t size) {
    return malloc(sizeof(void*) * size);
}

void readStubNameMapping(const char* filename) {
    symbol2Stub.clear();
    ifstream f(filename);
    string symbol, stub, overlay_name;
    while(f >> symbol) {
        assert(f >> stub);
        assert(f >> overlay_name); // discarded
        assert(symbol2Stub.find(symbol) == symbol2Stub.end());
        if (overlay_name != "-INTERNAL")
            symbol2Stub[symbol] = stub;
    }
    cout << "Read " << symbol2Stub.size() << " stub mappings" << endl;
}

void printBasicInfo(bfd* data) {
    cout << "BFD UID: " << data->id << endl;
    cout << "Filename " << data->filename << endl;
    cout << "Number of sections: " << data->section_count << endl;
    cout << "Direction: " << data->direction << endl;

    const bfd_target* t = data->xvec;
    cout << "Target name: " << t->name << endl;

    if (data->section_count == 0) return;

    cout << "Sections:" << endl;
    bfd_section *s = data->sections;
    while(true) {
        cout << "Id: " << s->id << \
             " index: " << s->index << \
             " name " << s->name << \
             " number of relocations " << s->reloc_count  << endl;
        //      s->name = "LOL";
        if (s == data->section_last) break;
        s = s->next;
    }
}

void addSomeSymbols(bfd* data) {
    int N = bfd_get_symtab_upper_bound(data);
    if (N <= 0) {
        cout << "WUUUT" << endl;
    }

    asymbol* s;
    asymbol **symtab;
    s = bfd_make_empty_symbol(data);
    s->name = "myprecioussymbol";
    s->section = data->sections;//bfd_make_section_old_way(data, ".mibs_symbols");
    s->flags = BSF_GLOBAL;
    s->value = 0x12345;

    symtab = (asymbol**) pmalloc(N + 1);
    int n = bfd_canonicalize_symtab(data, symtab);

    if (n < 0) cout << "LOOOOL" << endl;

    symtab[n] = s;
    symtab[n+1] = 0;


    cout << "Zapisuje do symtaba pozycji " << n+1 << endl;

    bfd_set_error(bfd_error_no_error);

    cout << bfd_errmsg(bfd_get_error()) << endl;
    if (!bfd_set_symtab(data, symtab, n+1)) {
        cout << "problem setting symtab" << endl;

        cout << bfd_errmsg(bfd_get_error()) << endl;
    }

    // bfd_set_symtab(data, symtab, n+1);
}

bool mtry(bool ok) {
    if (! ok) cout << bfd_errmsg(bfd_get_error()) << endl;
    return ok;
}

#define massert( x ) assert(mtry( (x) ))

const char* stub_section_name = "_mib_STUB";
asection* stub_section;

void setup_new_sections(bfd* ibfd, bfd* obfd) {
    assert(bfd_get_section_by_name(ibfd, stub_section_name) == NULL);
    stub_section = bfd_make_section_old_way(obfd, stub_section_name);
    stub_section->output_section = stub_section;
    stub_section->output_offset = 0;
    stub_section->lma = stub_section->vma = 0;
}

void create_new_sections(bfd* ibfd, bfd* obfd) {

}

int enter_new_symbols(asymbol** symtab_ptr, bfd* obfd) {
    set<string> stub_names;
    for(auto &x: symbol2Stub) {
        stub_names.insert(x.second);
    }

    for(auto &stub: stub_names) {
        asymbol *new_symbol = bfd_make_empty_symbol(obfd);
        new_symbol->name = stub.c_str();
        new_symbol->section = bfd_und_section_ptr;
        //new_symbol->section = stub_section;
        new_symbol->flags = BSF_GLOBAL;
        new_symbol->value = 0;

        (*symtab_ptr) = new_symbol;
        symtab_ptr++;
    }
    (*symtab_ptr) = 0;
    
    return stub_names.size();
}

void setup_section(bfd *ibfd, asection* isection, void* arg) {
    bfd* obfd = (bfd*) arg;
    asection* osection = bfd_make_section_anyway(obfd, bfd_section_name(ibfd, isection));
    assert(osection != NULL);

    assert(bfd_set_section_size(obfd, osection, bfd_section_size(ibfd, isection)));

    bfd_vma vma = bfd_section_vma (ibfd, isection);
    assert(bfd_set_section_vma(obfd, osection, vma));

    osection->lma = isection->lma;

    assert(bfd_set_section_alignment(obfd, osection, bfd_section_alignment(ibfd, isection)));

    flagword flags = bfd_get_section_flags(ibfd, isection);
    assert(bfd_set_section_flags(obfd, osection, flags));

    isection->output_section = osection;
    isection->output_offset = 0; // ?

    assert(bfd_copy_private_section_data(ibfd, isection, obfd, osection));
}

static asymbol** symtab;

void copy_relocations_in_section(bfd* ibfd, asection* isection, void* arg) {
    bfd* obfd = (bfd*) arg;

    bfd_size_type size = bfd_get_section_size(isection);

    if (size == 0) {
        cout << "Warning: section of size 0: " << isection->name << endl;
        return;
    }

    assert(isection != NULL);
    asection* osection = isection->output_section;

    int relsize = bfd_get_reloc_upper_bound(ibfd, isection);
    assert(relsize >= 0);

    if (relsize == 0) {
        return;
    }

    arelent** relocs = NULL;
    int relcount = 0;

    if (relsize > 0) {
        relocs = (arelent**) pmalloc(relsize);
        relcount = bfd_canonicalize_reloc(ibfd, isection, relocs, symtab);
        for(int i=0; i<relcount; ++i) {
            asymbol** orig_ptr = relocs[i]->sym_ptr_ptr;
            if (sym_ptr2Stub_ptr.find(orig_ptr) != sym_ptr2Stub_ptr.end())
                relocs[i]->sym_ptr_ptr = sym_ptr2Stub_ptr[orig_ptr];
        }
        massert(relcount >= 0);
    }

    bfd_set_reloc(obfd, osection, relocs, relcount);
    if (relsize == 0) {
        cout << "Section " << isection->name << " has no relocs" << endl;
        osection->flags &= ~SEC_RELOC;
    }
}

void copy_section(bfd *ibfd, asection* isection, void* arg) {
    bfd* obfd = (bfd*) arg;

    asection* osection = isection->output_section;
    bfd_size_type size = bfd_get_section_size(isection);

    if (size == 0) {
        cout << "Warning: section of size 0: " << isection->name << endl;
        return;
    }

    assert(size > 0);
    assert(osection != NULL);

    if (! bfd_get_section_flags(ibfd, isection) & SEC_HAS_CONTENTS) {
        cout << "Warning: section has no contents" << endl;
        return;
    }

    PTR memhunk = (PTR) malloc((unsigned) size);
    assert(memhunk != NULL);
    assert(bfd_get_section_contents(ibfd, isection, memhunk, (file_ptr)0, size));
    if (! bfd_set_section_contents(obfd, osection, memhunk, (file_ptr)0, size)) {
        cout << "Section not copied: isection name: " << isection->name << endl;
    }
    free(memhunk);
}

void fillInRelocRemapping(int n) {
    map<string, asymbol**> M;
    for(int i=0; i<n; ++i) {
        string name = symtab[i]->name;
        assert(M.find(name) == M.end());
        M[name] = symtab + i;
    }
    for(auto& x : symbol2Stub) {
        if (M.find(x.first) != M.end()) {
            cout << "Symbol found: " << x.first << endl;
            sym_ptr2Stub_ptr[M[x.first]] = M[x.second];
        } else {
            cout << "Symbol not found: " << x.first << endl;
        }
    }
    cout << "Reloc ptr remap: " << sym_ptr2Stub_ptr.size() << " entries" << endl;
}

void copy_object(bfd *ibfd, bfd *obfd) {
    int start = bfd_get_start_address(ibfd);

    cout << "Preparing object" << endl;

    massert(bfd_set_format (obfd, bfd_get_format(ibfd)));
    massert(bfd_set_start_address(obfd, start));
    massert(bfd_set_file_flags(obfd,
                               (bfd_get_file_flags(ibfd) & bfd_applicable_file_flags(obfd))));


    assert(bfd_set_arch_mach(obfd, bfd_get_arch(ibfd), bfd_get_mach(ibfd)));

    cout << "Section setup" << endl;
    // ALL OUTPUT SECTIONS SHOULD BE CREATED BEFORE ANY OUTPUT IS DONE
    bfd_map_over_sections(ibfd, setup_section, obfd);
    cout << "Creating new sections" << endl;
    setup_new_sections(ibfd, obfd);

    int additional_symbol_count_upper_bound = symbol2Stub.size() + 1; // upper bound

    int symsize = bfd_get_symtab_upper_bound(ibfd);
    assert(symsize >= 0);

    cout << "Allocating symtab of size " << symsize + additional_symbol_count_upper_bound << endl;
    symtab = (asymbol**) pmalloc(symsize + additional_symbol_count_upper_bound);
    cout << "Loading canonical symtab" << endl;
    int symcount = bfd_canonicalize_symtab(ibfd, symtab);
    assert(symcount >= 0);

    cout << "Entering new symbols" << endl;
    symcount += enter_new_symbols(symtab + symcount, obfd);
    cout << "Total of " << symcount << " symbols" << endl;
    assert(bfd_set_symtab(obfd, symtab, symcount));
    
    create_new_sections(ibfd, obfd);
    cout << "Building symbol mapping" << endl;
    fillInRelocRemapping(symcount);
    
    cout << "Remapping relocs" << endl;
    bfd_map_over_sections(ibfd, copy_relocations_in_section, obfd);
    cout << "Copying sections" << endl;
    bfd_map_over_sections(ibfd, copy_section, obfd);

    cout << "Copying private header / bfd data" << endl;
    assert(bfd_copy_private_header_data(ibfd, obfd));
    assert(bfd_copy_private_bfd_data (ibfd, obfd));
}

int main(int argc, char* argv[]) {
    cout << "Translating: " << argv[1] << endl;
    bfd_init();
    bfd* ibfd = bfd_openr(argv[1], NULL);
    bfd* obfd = bfd_openw(argv[2], "elf32-epiphany");
    readStubNameMapping(argv[3]);
    //bfd* data = bfd_fopen(argv[1], "elf32-epiphany", "r+", -1);

    assert(ibfd != NULL && obfd != NULL);

    if (!bfd_check_format(ibfd, bfd_object)) {
        cout << bfd_errmsg(bfd_get_error()) << endl;
        return 1;
    }

    //printBasicInfo(ibfd);
    copy_object(ibfd, obfd);

    cout << "Closing objects, performing writes" << endl;
    if (! bfd_close(obfd) ) {
        cout << "problem closing" << endl;
    }
    bfd_close(ibfd);

    return 0;
}

