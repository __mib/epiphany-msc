#!/usr/bin/python2.7
import sys, os

NOT_AN_OVERLAY = ['-', '-INTERNAL']

if __name__ == "__main__":
    f = open(sys.argv[1])
    M = [{'orig_name':a.strip(), 'stub_name':b.strip(), 'overlay_section_name':c.strip()} \
        for (a, b, c) in [x.split() for x in f] if c not in NOT_AN_OVERLAY]
    f.close()
    
    f = open(sys.argv[2])
    s = f.read()
    f.close()

    print '.file   "mib-stubs-generated.a"'
    for d in M:
        print s.format(**d)
