#include <stdlib.h>

int try_allocate() {
    void* p = malloc(1024 * sizeof(char));
    return (p != 0);
}

int Main(int n) {
    int allocated = 0;
    while(allocated < n) {
        if (! try_allocate()) break;
        ++ allocated;
    }
    return allocated;
}
