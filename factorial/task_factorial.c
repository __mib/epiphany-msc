int multiplication(int a, int b) {
    return a * b;
}

int factorial(int a) {
    if (a <= 1) return 1;
    return multiplication(a, factorial(a-1));
}

int main(int a) {
    return factorial(a);
}
